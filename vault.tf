module "vault-cluster" {
  source  = "achavacloud/workspaces/hashicorp"
  version = " 1.0.11"

  organization                  = data.tfe_organization.workspace.name
  create_project                = true
  project_name                  = "vault-ops"
  workspace_name                = "hashicorp-vault-cluster-nonprod"
  identifier                    = "achavacloud/enterprise-hashicorp/vault/hashicorp-vault-cluster-config"
  oauth_token_id                = var.oauth_token_id
  branch                        = "main"
  terraform_version             = "1.9.6"
  auto_apply                    = true
  assessments_enabled           = true
  queue_all_runs                = true
  working_directory             = "cluster"
  structured_run_output_enabled = true
  tags                          = ["nonprod", "vault-ops"]

  enable_run_trigger = false
}

module "vault-infra-config" {
  source  = "achavacloud/workspaces/hashicorp"
  version = " 1.0.11"

  organization                  = data.tfe_organization.workspace.name
  create_project                = false
  project_name                  = "vault-ops"
  workspace_name                = "hashicorp-vault-infra-config-nonprod"
  identifier                    = "achavacloud/enterprise-hashicorp/vault/hashicorp-vault-cluster-config"
  oauth_token_id                = var.oauth_token_id
  branch                        = "main"
  terraform_version             = "1.9.6"
  auto_apply                    = true
  assessments_enabled           = true
  queue_all_runs                = true
  working_directory             = "infra"
  structured_run_output_enabled = true
  tags                          = ["nonprod", "vault-ops"]

  enable_run_trigger  = true
  source_workspace_id = module.vault-cluster.workspace_id
}

module "vault-config" {
  source  = "achavacloud/workspaces/hashicorp"
  version = " 1.0.11"

  organization                  = data.tfe_organization.workspace.name
  create_project                = false
  project_name                  = "vault-ops"
  workspace_name                = "hashicorp-vault-config-nonprod"
  identifier                    = "achavacloud/enterprise-hashicorp/vault/hashicorp-vault-cluster-config"
  oauth_token_id                = var.oauth_token_id
  branch                        = "main"
  terraform_version             = "1.9.6"
  auto_apply                    = true
  assessments_enabled           = true
  queue_all_runs                = true
  working_directory             = "config"
  structured_run_output_enabled = true
  tags                          = ["nonprod", "vault-ops"]

  enable_run_trigger  = true
  source_workspace_id = module.vault-policies.workspace_id
}

module "vault-policies" {
  source  = "achavacloud/workspaces/hashicorp"
  version = " 1.0.11"

  organization                  = data.tfe_organization.workspace.name
  create_project                = false
  project_name                  = "vault-ops"
  workspace_name                = "hashicorp-vault-policies-nonprod"
  identifier                    = "achavacloud/enterprise-hashicorp/vault/hashicorp-vault-cluster-config"
  oauth_token_id                = var.oauth_token_id
  branch                        = "main"
  terraform_version             = "1.9.6"
  auto_apply                    = true
  assessments_enabled           = true
  queue_all_runs                = true
  working_directory             = "policies"
  structured_run_output_enabled = true
  tags                          = ["nonprod", "vault-ops"]

  enable_run_trigger  = true
  source_workspace_id = module.vault-infra-config.workspace_id
}
