terraform {
  backend "remote" {
    organization = "achavacloud"
    hostname     = "app.terraform.io"
    workspaces {
      name = "enterprise-workspaces"
    }
  }
  required_providers {
    vault = {
      source  = "hashicorp/vault"
      version = "3.25.0"
    }
  }
}
