variable "tfe_token" {
  description = "The Terraform Cloud API token."
  type        = string
}

variable "organization" {
  description = "The name of the Terraform Cloud organization."
  type        = string
  default     = "achavacloud"
}

variable "oauth_token_id" {
  description = "The OAuth token ID for VCS integration. This token allows Terraform Cloud to access the specified VCS repository securely."
  type        = string
  default     = ""
}
